'''
Estruturas de Dados no Python
Tupla
Dicionário
Conjunto

Diego Mendes Rodrigues
'''

# Lista, vista anteriormente (list)
# ---------------
# Cada item possui sua posição, podemos adicionar, remover itens
# Dinâmica, ordenada
minha_lista = ['Diego', 'Regina', 'Bruna']


# Tupla (tuple)
# ---------------
# A Tupla não é mutável como a Lista
# Não existe um método .append(), nem .extend(), nem .remove()
# Existem os métodos .count() e .index()
# Podemos pegar o valor dos objetos, modificar o valor dos objetos, mas não podemos remover ítens
# Quando utilizar? Quando precisar de um número limitado de opções, que não será alterada
minha_tupla = ('Natalia', 'Jerome')


# Dicionário (dict)
# ---------------
# Funciona como um Dicionário que temos em casa, cada elemente tem uma Chave (key) e um Valor (value) (significado)
# Em outras linguagens é conhecido como hashmap, hashtable (Java, C#)
meu_dicionario = {'nome':'Diego','idade':34,'altura':1.7,'cpf':'220.473.318-03'}


# Conjunto (set)
# ---------------
# Uma Lista misturada com Dicionário
#-
# Qual a diferença de um Conjunto para uma Lista?
# -> No conjunto não existem itens repetidos, caso eu adicione um item repedido, não acontece nada
meu_conjunto = {'Ana','Diego', 'Regina', 'Bruna'}
print(meu_conjunto)
meu_conjunto = {'Ana','Diego', 'Regina', 'Bruna','Ana','Sergio'}
print(meu_conjunto) # Ana não aparece 2 vezes
# -> O Conjunto é Dinâmico como a Lista, podemos adicionar e remover itens .add(), .clear(), .remove()...
# -> Conjunto NÃO é ordenado, NÃO podemos pegar o [0], o [1]...


# ANALISANDO
# ---------------

# TUPLA
print('\nTUPLA')
print(minha_tupla)
print(type(minha_tupla))
print(minha_tupla[0])
print(minha_tupla[1])

for item in minha_tupla:
    print(item);

# Substituindo - Precisamos substituir a Tupla INTEIRA, embora seja costume utilizar a Tupla Estática
minha_tupla = ('Julia', 'Natalia','Regina')
print(minha_tupla)

# Perguntar se um objeto está dentro da Tupla
# Podemos fazer com listas, conjuntos e etc
# No dicionário a pesquisa é diferente
if 'Julia' in minha_tupla:
    print('Julia está na Tupla (coleção)!')
else:
    print('Julia não está na Tupla...')

#qtd_tupla = minha_tupla.count(0)
#print('Elementos na Tupla',qtd_tupla)


# DICIONÁRIO
print('\nDICIONÁRIO')
print(meu_dicionario)
print(type(meu_dicionario))

print(meu_dicionario['nome'])
print(meu_dicionario['idade'])
print(meu_dicionario['cpf'])

#print(meu_dicionario[1]) # Erro, pois precisamos usar a chave

# Usando len() para contar quantos ítens possuímos no Dicionário
print('Ítens no Dicionário: ' + str(len(meu_dicionario)) )

# Nas buscas, o Dicinário e o Conjunto são muito mais eficientes que Listas e Tuplas
# Busca
if 'Diego' in meu_dicionario.values():
    print('Diego está na lista de valores do dicionário')
else:
    print('Diego Não está na lista de valores do dicionário')

if 'Paula' in meu_dicionario.values():
    print('Paula está na lista de valores do dicionário')
else:
    print('Paula Não está na lista de valores do dicionário')

# Mostrando todos os valores do Dicionário com FOR
print('\n')
for valor in meu_dicionario.values():
    print(valor);

# Mostrando todos as Chaves do Dicionário com FOR
print('\n')
for valor in meu_dicionario.keys():
    print(valor);

# Alterando Valores do Dicionário
meu_dicionario['nome'] = 'Regina'
meu_dicionario['cpf'] = '123.544.737-08'
meu_dicionario['altura'] = 1.65
meu_dicionario['idade'] = 54
print('\n')
print(meu_dicionario);

# Adicionando ítens ao Dicionário
meu_dicionario['endereco'] = 'Av. Paulista, 1127'
meu_dicionario['telefone'] = '(19) 4326-9872'
print('\n')
print(meu_dicionario)

# Podemos usar .pop(), .clear(), .update()...

# CONJUNTO
print('\nCONJUNTO')
print(meu_conjunto)
print(type(meu_conjunto))

# print(meu_conjunto[0]) # Não funciona, pois o Conjunto não está ordenado. Quem está ordenada é a Lista.

# Adicionando
meu_conjunto.add('Jerome')
print(meu_conjunto)

# Adicionando um nome reptido (NÃO funciona)
meu_conjunto.add('Diego')
print(meu_conjunto)

# Buscando dentro do Conjunto
if 'Regina' in meu_conjunto:
    print('Regina foi encontrada dentro do Conjunto')
else:
    print('Regina NÃO foi encontrada dentro do Conjunto')

# A diferença de performance entre
# if 'Regina' in meu_conjunto:
# e
# if 'Regina' in minha_lista:
# é MUITO GRANDE!!!
# -
# A busca nos Conjuntos é MUITO MAIS RÁPIDA
# -
# Pq?
# Quando fazemos a pergunta na Lista, o Python vai percorrer a Lista inteira buscando o Valor, se tiver 100.000.000 de
# itens o Python irá fazer 100.000.000 de ações buscando. A Lista é Ordenada, ela não é feita para realizarmos buscas.
# Já o Conjunto é uma 'tabela hash', ou seja, cada palavra é transformada em um código colocado em uma tabela. Essa
# tabela especial fica na memória e busca é praticamente instantânea. Mesmo se tivermos 100.000.000 de elementos no
# conjunto, a resposta será praticamente instantânea.
# -
# Pesquisa -> Utilizar Conjuntos ou Dicionários
# -

# Podemos usar .remove()... em Conjuntos


# Como inicializar vazio
# -------
lista = []
tupla = ()
dicionario = {}
conjunto = set()


# Podemos colocar uma estrutura de dados dentro de outra
# Colocando Tuplas dentro de uma Lista
lista_numeros = [(1,2), (3,4), (5,6)]
print(lista_numeros)

# Colocando Tuplas e Conjuntos dentro de uma Lista
lista_numeros_nomes = [(1,2), (3,4), (5,6), ({'Ana', 'Maria'}, {'João'})]
print(lista_numeros_nomes)