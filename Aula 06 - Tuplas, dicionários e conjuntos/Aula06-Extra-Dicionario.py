'''
Dicionários (set())

Fonte: http://www.programeempython.com.br/blog/estruturas-de-dados-em-python-dicionarios/

Diego Mendes Rodrigues
'''

# Os dicionários em Python são estruturas de dados que contém pares de chave-valor.


# Primeiro definimos um dicionário vazio:
dicionario = {}


# Dicionário que relacionasse strings para os dias da semana, como 'seg' e 'ter' com números
dias_da_semana = {'seg':0, 'ter':1, 'qua':2, 'qui':3, 'sex':4, 'sab':5, 'dom':6}
print(dias_da_semana)
print(dias_da_semana['ter'])
print(dias_da_semana['dom'])


# O método dict() permite transformar listas de tuplas em dicionários
# -------------
# Vamos criar um dicionário que relacionas as letras N, L, S, O dos pontos cardeais com números
pontos_cardeais = dict( [('N', 0), ('L', 1), ('S', 2), ('O', 3)] )
print('\n')
print(pontos_cardeais)
print(pontos_cardeais['N'])
print(pontos_cardeais['O'])


# Montar um dicionário que relaciona os inteiros de zero a dez e seus respectivos quadrados
quadrados = dict( [(x,x**2) for x in range(11)] )
print('\n')
print(quadrados)
print(quadrados[7])


# Verificando se existe uma Chave no Dicionário
print('\n')
if 'seg' in dias_da_semana:
    print('seg está nesse dicionário')
else:
    print('seg NÃO está nesse dicionário')

if 'fri' in dias_da_semana:
    print('fri está nesse dicionário')
else:
    print('fri NÃO está nesse dicionário')


# Verificando se NÃO existe uma Chave no Dicionário
print('\n')
if 'seg' not in dias_da_semana:
    print('seg NÃO está nesse dicionário')
else:
    print('seg está nesse dicionário')

if 'fri' not in dias_da_semana:
    print('fri NÃO está nesse dicionário')
else:
    print('fri está nesse dicionário')


# Adicionando chaves/valores no Dicionário
print('\n')
print(dias_da_semana)
dias_da_semana['Seg']=7
dias_da_semana['Ter']=8
print(dias_da_semana)

# Removendo chaves/valores do Dicionário
print('\n')
print(dias_da_semana)
del dias_da_semana['Seg']
print(dias_da_semana)
del dias_da_semana['Ter']
print(dias_da_semana)

# Exibir quais Chaves existem no dicionário
print('\n')
print( dias_da_semana.keys() )

# Exibir quais Valores existem no dicionário
print('\n')
print( dias_da_semana.values() )