'''
Utilizando os operados E (and) e OU (or)
É necessário escrever, não usamos simbolos como em Linguagem C

Diego Mendes Rodrigues
'''

print(1==1 and 1==2)
print(1==1 or 1==2)

print('\nTr')
idade = input('Sua idade: ')
idade = int(idade)

if idade >= 18 and idade < 65:
    print('Adulto')
elif idade >= 65:
    print('3a idade')
else:
    print('Menor de idade')