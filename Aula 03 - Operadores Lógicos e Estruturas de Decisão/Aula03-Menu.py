'''
Criando um Menu e analisando a resposta com IF

Diego Mendes Rodrigues
'''

print('Menu')
print('1 - Escrever Diego')
print('2 - Escrever Regina')
print('3 - Escrever Bruna\n')

opcao = input('Selecione a opção: ')
opcao = int(opcao)

print('\n')
if opcao == 1:
    print('Diego')
elif opcao == 2:
    print('Regina')
elif opcao == 3:
    print('Bruna')
else:
    print('Opção inválida')