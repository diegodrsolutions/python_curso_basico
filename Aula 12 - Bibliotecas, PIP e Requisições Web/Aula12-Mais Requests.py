'''
Analisando a biblioteca requests

Fonte: http://www.pythonforbeginners.com/requests/using-requests-in-python

Fonte PT-BR: http://docs.python-requests.org/pt_BR/latest/

Diego Mendes Rodrigues
'''

import requests

# Fazendo uma requisição GET
# --------------------------------
req = requests.get('https://github.com/timeline.json')
print(req.status_code)
print('')

# Verificando o Status Code
if req.status_code == requests.codes.ok:
    print('Página descarregada com sucesso (200)\n')

if req.status_code == requests.codes['temporary_redirect']:
    print('Página Redirecinada (307)\n')

if req.status_code == requests.codes['gone']:
    print('Gone (410)\n')

print(req.text)
print(req.json)
print('')

# Fazendo outra requisição GET
# --------------------------------
req = requests.get('https://github.com/blog/1160-github-api-v2-end-of-life')

print('--------------------------------------------------------')
print(req.status_code)
print('')

# Verificando o Status Code
if req.status_code == requests.codes.ok:
    print('Página descarregada com sucesso (200)\n')

print(req.text)
print(req.json)

print('')
print('--------------------------------------------------------')
print('CABEÇALHOS')
print('--------------------------------------------------------')

cabecalhos = req.headers
print(cabecalhos)

print('')
print(req.headers.get('content-type'))

print('')
print('--------------------------------------------------------')
print('ENCODING')
print('--------------------------------------------------------')
print(req.encoding)

print('')
print('--------------------------------------------------------')
print('JSON')
print('--------------------------------------------------------')
import json
url = 'https://api.github.com/some/endpoint'
payload = {'some': 'data'}
headers = {'content-type': 'application/json'}
r = requests.post(url, data=json.dumps(payload), headers=headers)

print('')
print('--------------------------------------------------------')
print('OUTRAS REQUISIÇÕES')
print('--------------------------------------------------------')
'''
Outras Requisições
r = requests.put("http://httpbin.org/put")
r = requests.delete("http://httpbin.org/delete")
r = requests.head("http://httpbin.org/get")
r = requests.options("http://httpbin.org/get")
'''