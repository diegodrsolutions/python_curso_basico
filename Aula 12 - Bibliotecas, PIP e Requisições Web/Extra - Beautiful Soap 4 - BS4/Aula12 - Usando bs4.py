'''
Beautiful Soup 4 (ou BS4)
-
Beautiful Soup is a Python library for pulling data out of HTML and XML files. It works with your favorite parser to
provide idiomatic ways of navigating, searching, and modifying the parse tree. It commonly saves programmers hours
or days of work.

Sites:
https://www.crummy.com/software/BeautifulSoup/bs4/doc/
http://www.pythonforbeginners.com/beautifulsoup/beautifulsoup-4-python

Instalar:
pip install bs4

Diego Mendes Rodrigues
'''

import requests
from bs4 import BeautifulSoup

site = 'http://www.uol.com.br'

# Fazendo uma requisição GET
# --------------------------------
req = requests.get(site)
pagina_html = req.text
req.close()

print('')
soup = BeautifulSoup(pagina_html, 'html.parser')

# Links <a ...>
links = soup.find_all('a')
for item in links:
    print(item)
print('')

# Titulos <h1 ...>
h1 = soup.find_all('h1')
for item in h1:
    print(item)
print('')

# Titulos <h2 ...>
h2 = soup.find_all('h2')
for item in h2:
    print(item)
print('')

# Titulos <h3 ...>
h3 = soup.find_all('h3')
for item in h3:
    print(item)
print('')

# Negritos <b ...>
b = soup.find_all('b')
for item in b:
    print(item)
print('')

# Todos os Titulos
# Expressão regular pegando todas as TAGs que começam com h (html, header, head, h1, h2...)
import re
for tag in soup.find_all(re.compile("^h")):
    print(tag.name)
print('')

# Título da página
titulo = soup.title.string
print(titulo)