'''
Requisições Web

Utilizaremos o site PutsReq.com
PutsReq lets you record HTTP requests and simulate responses like no other tool available.

Ver também: http://docs.python-requests.org/pt_BR/latest/

Diego Mendes Rodrigues
'''

import requests

requisicao = None
texto = None

try:
    requisicao = requests.get('http://putsreq.com/OEV3KFieLLDiOrVC88xB')
    #requisicao = requests.post('http://putsreq.com/OEV3KFieLLDiOrVC88xB')
    texto = requisicao.text
except Exception as erro:
    print('Requisição deu erro:', erro)
    quit()

print(texto)


# Criando um cabeçalho
meus_cabecalhos = {'User-agent':'Windows 10.0',
             'Referer':'https://google.com'}

meus_cookies = {'Ultima-visita':'10-10-2020'}

meus_dados = {'username':'diego',
         'senha':'12345678',
         'name': 'Diego Mendes Rodrigues'}

try:
    requisicao = requests.post('http://putsreq.com/OEV3KFieLLDiOrVC88xB',
                              headers=meus_cabecalhos,
                              cookies=meus_cookies,
                              data=meus_dados)
    texto = requisicao.text
except Exception as erro:
    print('Requisição deu erro:', erro)
    quit()

print(texto)