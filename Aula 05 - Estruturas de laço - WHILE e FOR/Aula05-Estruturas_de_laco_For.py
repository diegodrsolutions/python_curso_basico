'''
Estruturas de Laço no Python
FOR

Diego Mendes Rodrigues
'''

# Usaremos uma Lista para percorrer essa lista com a estrutura de laço
nomes = ['Diego', 'Regina', 'Bruna', 'Natalia', 'Sergio', 'Jerome', 'Julia']
nomes.sort()

# Lista original
print('Lista:')
print(nomes)

# FOR (diferente da Linguagem C)
# FOR (item dentro de uma coleção)
# for <item> in <coleção>:
# -
# Exibindo os nomes, um a um
print('\nExibindo os nomes:')
for nome in nomes:
    print(nome)

# range()
# for
# Exibindo uma lista de números
print('\nExibindo uma lista de números:')
lista_numeros = range(5)
for item in lista_numeros:
    print(item)

print('\n')
lista_numeros = range(3,9) # Colocando intervalo no range()
for item in lista_numeros:
    print(item)

print('\n')
lista_numeros = range(0,20,4) # Colocando intervalo e passo no range()
for item in lista_numeros:
    print(item)

print('\n')
for item in range(0,20,2):
    print(item)

# Misturando for, range e a Lista nomes
print('\nExibindo a lista com range():')
for i in range(7):
    print(nomes[i])

# -
# Melhor usar o len para determinar o tamanho da Lista
# len(nomes) retorna o tamanho da Lista nomes
print('\nExibindo a lista com range() e len():')
for i in range(len(nomes)):
    print(nomes[i])

print('\nExibindo a lista com range(), len() e adicionando:')
for i in range(len(nomes)):
    print(nomes[i])
    nomes.append('Oi')
print(nomes)

# Removendo Oi
nomes = nomes[0:7]
print(nomes)

# Usando o FOR em uma string
print('\nFOR numa String:')
curso = 'Python Básico'
for letra in curso:
    print(letra)