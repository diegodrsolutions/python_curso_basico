'''
Estruturas de Laço no Python
WHILE (enquanto)

Diego Mendes Rodrigues
'''

# Usaremos uma Lista para percorrer essa lista com a estrutura de laço
nomes = ['Diego', 'Regina', 'Bruna', 'Natalia', 'Sergio', 'Jerome', 'Julia']
nomes.sort()

# Lista original
print('Lista:')
print(nomes)

# Usando um WHILE
print('\n')
i=0
while i < 10:
    print('i é menor que 10, i =', i)
    i += 1
print('Acabou o while, i =', i)

# Usando WHILE para mostrar nomes
print('\nNomes com while:')
i=0
while i < len(nomes):
    print(nomes[i])
    i += 1

# Usando o BREAK
print('\nParando o while infinito com break:')
i=0
while True:
    print(i)
    if i == 6:
        break
    i += 1
print('Saiu do while!!')