# README #

## Sobre o curso ##

O curso Python Básico é um curso da linguagem de programação Python e lógica de programação para iniciantes, ideal para quem nunca programou ou está querendo migrar para a linguagem Python.
O aluno irá aprender a linguagem Python desde o zero absoluto, passará por todos os conceitos principais da linguagem e lógica até a criação de programas mais complexos e de real utilidade.

É um curso que pode ser feito por iniciantes, entusiastas e desenvolvedores de qualquer idade e experiência.

Trata-se de um curso extremamente completo, detalhado, atualizado, didático e prático. Conta com um sistema de aprendizado passo a passo, que de maneira leve e intuitiva leva o aluno ao aprendizado fácil.
O curso é 100% gratuito e conta com certificado e suporte.

### Curso Python Básico ###

* Versão 1.0
* [Solyd](http://ensino.solyd.com.br/login/index.php)