'''
Minha Biblioteca que automatiza a pesquisa no Twitter

Diego Mendes Rodrigues
'''

import oauth2
import json
import urllib.parse

class Twitter:
    # Construtor
    def __init__(self, Consumer_Key:str, Consumer_Secret:str, Token_Key:str, Token_Secret:str):
        self.conexao(Consumer_Key, Consumer_Secret, Token_Key, Token_Secret)

    def conexao(self, Consumer_Key:str, Consumer_Secret:str, Token_Key:str, Token_Secret:str):
        self.consumidor = oauth2.Consumer(Consumer_Key, Consumer_Secret)
        self.token = oauth2.Token(Token_Key, Token_Secret)
        self.cliente = oauth2.Client(self.consumidor, self.token)
        return self.cliente

    def tweet(self, texto:str):
        texto_sem_espacos = urllib.parse.quote(texto, safe='')

        # Agora precisa ser POST
        req = self.cliente.request('https://api.twitter.com/1.1/statuses/update.json?status=' + texto_sem_espacos,
                                    method='POST')

        # Transformando em str
        decodificar = req[1].decode()

        # Tranaformando em um dicionário
        dicionario = json.loads(decodificar)
        return dicionario

    def pesquisar(self,texto:str, lingua:str='pt'):
        texto_sem_espacos = urllib.parse.quote(texto, safe='')

        req = self.cliente.request('https://api.twitter.com/1.1/search/tweets.json?q=' + texto_sem_espacos + '&lang=' + lingua)
        # req é uma tupla (objeto e bytes)

        # Transformando em str
        decodificar = req[1].decode()

        # Tranaformando em um dicionário
        dicionario = json.loads(decodificar)

        # Essa resposta do statuses me retorna uma lista
        respostas = dicionario['statuses']
        return respostas