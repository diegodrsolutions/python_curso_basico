'''
Utilizando expressões regulares para procurar e-mails

Expressões regulares (RegEx):
Procurar por pradrões dentro de textos

https://regex101.com/

Padrões:
 - E-mail:
 email@dominio.com.br

 - Telefones:
 99 999999999
 99 99999 9999

 - IPs:
 999.999.999.999

Diego Mendes Rodrigues
'''

# Biblioteca de Expressões Regulares (RegEx)
import re

# Função que realiza a busca da RegEx na frase
# --------------------------------------------------
def realizar_busca(texto,frase):
    padrao = re.search(str(texto), frase)  # r RAW String
    print('Buscando search(): r\'' + texto + '\'')
    if padrao:
        print(padrao.group())
        print('\n')
    else:
        print('Padrão não encontrado\n')


# Função que realiza a busca de todas as ocorrencias da RegEx na frase
# --------------------------------------------------
def realizar_busca_todas(texto, frase):
    padrao = re.findall(str(texto), frase)  # r RAW String
    print('Buscando findall(): r\'' + texto + '\'')
    if padrao:
        print(padrao) # lista
        print('\n')
    else:
        print('Padrão não encontrado\n')



# print(r'') RAW String
print('Oi Diego.\nTudo Bem?')
print(r'Oi Diego.\nTudo Bem?')



# Busca com re.search(...)
# --------------------------------------------------

frase = 'O gato é bonito'
print(frase)
print('')

# Não coloquei padrão
realizar_busca(r'', frase)

# Padrão 'ga'
realizar_busca(r'ga', frase)

# Padrão 'gata'
realizar_busca(r'gata', frase)

# Padrão 'gat.' ou seja, gat+<qualquer caracter>
realizar_busca(r'gat.', frase)

# Padrão 'gat\w' ou seja, gat+<a-z+A-Z+0-9+_> ==> Não pega espaço em branco
realizar_busca(r'gat\w', frase)

# Padrão 'gat\w\w' ou seja, gat+<a-z+A-Z+0-9+_> ==> Não pega espaço em branco
realizar_busca(r'gat\w\w', frase)

# Padrão '\w\w\w\w' ou seja, 4 letras consectivas
realizar_busca(r'\w\w\w\w', frase)

# Padrão '\w\w\w\w.\w' ou seja, 4 letras, 1 caracter, 1 letra
realizar_busca(r'\w\w\w\w.\w', frase)



# Busca com re.findall(...)
# --------------------------------------------------

frase = 'O gato, a gata, os gatinhos, os gatões, inclusive o gat!'
print(frase)
print('')

# Padrão 'gat\w' ou seja, gat+1 letra
realizar_busca_todas(r'gat\w', frase)

# Padrão 'gat\w+' ou seja, gat+<uma ou mais letras que forem encontradas antes do espaço>
realizar_busca_todas(r'gat\w+', frase)

# Padrão 'gat\w*' ou seja, gat+<zero ou mais letras que forem encontradas antes do espaço>
# Como é de zero até infinito \w*, o zero letras faz pegar o 'gat' que o \w+ não pegou
realizar_busca_todas(r'gat\w*', frase)

# Padrão 'gat\w*..*'
realizar_busca_todas(r'gat\w*..*', frase)

# Caso eu queira pegar o ponto final (.), preciso escapar
# r'casa\.'

# Padrão '[gat]', buscando letras g a t
realizar_busca_todas(r'[gat]', frase)

# Padrão '[gat]+', buscando 1 ou mais letras g a t e palavras gat+[g a t]
realizar_busca_todas(r'[gat]+', frase)

# Padrão '[gat]+\w+', buscando 1 ou mais letras g a t e palavras gat+[g a t]+<uma ou mais letras que forem encontradas
# antes do espaço>
realizar_busca_todas(r'[gat]+\w+', frase)

# Padrão '\w{4,6}', palavras de 4 a 6 letras
realizar_busca_todas(r'\w{4,6}', frase)

# Usando o https://regex101.com/
# -------------------------------------------
'''
RegEx que encontra e-mails:
[\w\.-]+@[\w-]+\.[\w\.-]+

Testada no https://regex101.com/ com os seguintes dados
loren ipsum
teste@teste.com
diego.m.rodrigues@gmail.com
ana.maria@lab.fisica.usp.br
rita.pimentel@sei.ba.gov.br
rmrm80@yahoo.com.br
h0_h0@hotmail.com
bruna-mendes@afip.laboratorio.com.br
aa@cs.com
08071982@geo.io
meu texto aqui
'''

frase  = 'loren ipsum teste@teste.com diego.m.rodrigues@gmail.com ana.maria@lab.fisica.usp.br '
frase += 'rita.pimentel@sei.ba.gov.br rmrm80@yahoo.com.br h0_h0@hotmail.com bruna-mendes@afip.laboratorio.com.br'
frase += 'aa@cs.com 08071982@geo.io meu texto aqui'
print(frase)
print('')

# Padrão '[\w\.-]+@[\w-]+\.[\w\.-]+', ou seja, e-mails
realizar_busca_todas(r'[\w\.-]+@[\w-]+\.[\w\.-]+', frase)