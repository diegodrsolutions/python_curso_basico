'''
Consultando a Previsão do Tempo

OpenWeatherMap
openweathermap.org/api

API fechada, preciso realizar um cadastro
Price -> Free -> Get API Key and Start
User.........: h0_h0
E-mail usado.: h0_h0@hotmail.com
Senha........: diego123
Full name....: Diego

Key: d2a672e5d9ec97b325249e3caa8380cc

Example of API call:

api.openweathermap.org/data/2.5/forecast/city?id=524901&APPID=KEY_AQUI
api.openweathermap.org/data/2.5/forecast/city?id=CIDADE_AQUI&APPID=KEY_AQUI
Cidades:
http://openweathermap.org/city/3454690 - Paulínia
http://openweathermap.org/city/3467865 - Campinas
http://openweathermap.org/city/7521912 - São Paulo

api.openweathermap.org/data/2.5/forecast/city?id=3454690&APPID=d2a672e5d9ec97b325249e3caa8380cc

Diego Mendes Rodrigues
'''

import requests
import json
from datetime import *
import time

while True:
    try:
        # Realizando a requisição
        chave  = 'd2a672e5d9ec97b325249e3caa8380cc'
        cidade = '3454690'
        req = requests.get('http://api.openweathermap.org/data/2.5/forecast/city?id='+cidade+'?id=&APPID='+chave)
        conteudo_json = req.text
        #print(conteudo_json)

        # Convertendo o resultado em um Dicionário JSON
        dicionario = json.loads(conteudo_json)

        # Pegando o valor da Previsão do Tempo
        valores = dicionario['city']
        #print(valores)

        cidade_nome       = dicionario['city']['name']
        cidade_lista      = dicionario['list'][0]
        cidade_previsao   = cidade_lista['main']['temp']
        cidade_previsao_c = float(cidade_previsao) - 273.15

        print('Cidade......:', cidade_nome)
        print('Previsão K..:', cidade_previsao)
        print('Previsão °C.: %.2f' %(cidade_previsao_c))


    except Exception as erro:
        print('Erro ao conectar ao site api.promasters.net.br:')
        print(erro)

    data_hora_agora = datetime.now()
    print('%d/%d/%d - %d:%d:%d' %(data_hora_agora.day, data_hora_agora.month, data_hora_agora.year,
                                      data_hora_agora.hour, data_hora_agora.minute, data_hora_agora.second))

    print('\nNova consulta em 10s\n<Ctrl>+C ou <Ctrl>+<F12> para sair\n')
    time.sleep(10)

