'''
Fazer um programa que se conecta na Google Translate API‎
Digito texto -> tradução

pip install virtualenv
pip install google-cloud-translate

Endereço usado na criação da conta:
diego.m.rodrigues@gmail.com
Cartão: Nubank
Data: 24/11/2016

Google Cloud Translation API
ATIVAR
https://console.cloud.google.com/iam-admin/serviceaccounts/project?project=traducao-em-python
Key: AIzaSyBgzT_-LbKnUcTxg_tyJHsEi8B_rku-O58

https://cloud.google.com/translate/docs/translating-text

https://translation.googleapis.com/language/translate/v2?key=AIzaSyBgzT_-LbKnUcTxg_tyJHsEi8B_rku-O58&source=en&target=de&q=Hello%20world&q=My%20name%20is%20Jeff

https://translation.googleapis.com/language/translate/v2?key=AIzaSyBgzT_-LbKnUcTxg_tyJHsEi8B_rku-O58&source=pt-br&target=en&q=Minha%20Casa

GET https://www.googleapis.com/language/translate/v2?q=casa&target=en&source=pt-br&key={YOUR_API_KEY}

Diego Mendes Rodrigues
'''

import requests
import json

# Configurações da Tradução (linguas)
lingua_origem = 'pt-br'
lingua_destino = 'en'

def traduzir(texto):
    try:
        # Configurações da Tradução
        texto_original  = texto
        texto_traduzido = ''
        chave           = 'AIzaSyBgzT_-LbKnUcTxg_tyJHsEi8B_rku-O58'

        # URL da tradução
        texto_original_sem_espacos = texto_original.replace(' ','%20')
        site  = 'https://translation.googleapis.com/language/translate/v2?key='+ chave +'&source=' + lingua_origem
        site += '&target=' + lingua_destino + '&q=' + texto_original_sem_espacos

        # Realizando a requisição
        req = requests.get(site)
        retorno = json.loads(req.text)

        texto_traduzido = retorno['data']['translations'][0]['translatedText']

        return texto_traduzido

    except Exception as erro:
        print('Erro na tradução:')
        print(erro)
        exit()


while True:
    texto = input('Texto que será traduzido (<ENTER> para sair): ')
    if len(texto) <= 0:
        exit()

    texto_traduzido = traduzir(texto)

    print('')
    print('Original ('+lingua_origem+').:', texto)
    print('Traduzido ('+lingua_destino +')...:', texto_traduzido)
    print('')