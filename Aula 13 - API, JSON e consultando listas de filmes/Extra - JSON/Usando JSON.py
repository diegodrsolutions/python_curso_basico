'''
Usando a biblioteca JSON

Fonte: https://docs.python.org/3/tutorial/inputoutput.html#saving-structured-data-with-json
       https://docs.python.org/3/library/json.html#module-json

Diego Mendes Rodrigues
'''

import json
import requests

# json.dumps() - Serialize ``obj`` to a JSON formatted ``str``
# -----------------------------------------------------------------------
print('json.dumps()\n-------------------------')
objeto_json = json.dumps([1, 'Diego', 34])
print(objeto_json)
print('\n')


# json.dump() - Serialize ``obj`` as a JSON formatted stream to ``fp`` (a ``.write()``-supporting file-like object).
# -----------------------------------------------------------------------
print('json.dump()\n-------------------------')
arquivo  = 'C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\'
arquivo += 'Aula 13 - API, JSON e consultando listas de filmes\\Extra - JSON\\cadastro.json'
cadastro = open(arquivo,'w')

objeto = {"Cadastro":[
    {"id":"1", "nome":"Diego", "idade":"34"},
    {"id":"2", "nome":"Regina", "idade":"54"},
    {"id":"3", "nome":"Bruna", "idade":"28"}
]}


json.dump(objeto, cadastro)
#json.dump(objeto, cadastro, skipkeys=True)

cadastro.close()
print('Informações armazenadas em cadastro.json')
print(objeto)
print('\n')

# json.load() - Deserialize fp (a .read()-supporting file-like object containing a JSON document) to a Python object
# -----------------------------------------------------------------------
print('json.load()\n-------------------------')
arquivo  = 'C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\'
arquivo += 'Aula 13 - API, JSON e consultando listas de filmes\\Extra - JSON\\cadastro.json'
cadastro = open(arquivo,'r')

objeto = json.load(cadastro)
dados = objeto['Cadastro']

print('Informações lidas de cadastro.json')
for linha in dados:
    print('Id....:', linha['id'])
    print('Nome..:', linha['nome'])
    print('Idade.:', linha['idade'])
    print('')

cadastro.close()
print('')


# json.loads() - Deserialize fp (a .read()-supporting file-like object containing a JSON document) to a Python object
# -----------------------------------------------------------------------
print('json.loads() - www.omdbapi.com\n-------------------------')
filme='the matrix'
req = requests.get('http://www.omdbapi.com/?t='+filme+'&type=movie')
dados = req.text

dicionario = json.loads(dados)

print('Titulo......:', dicionario['Title'])
print('Ano.........:', dicionario['Year'])
print('Lançamento..:', dicionario['Released'])
print('Duração.....:', dicionario['Runtime'])
print('Genero......:', dicionario['Genre'])
print('Atores......:', dicionario['Actors'])
print('Diretor.....:', dicionario['Director'])
print('Nota (IMDb).:', dicionario['imdbRating'])
print('Resumo......:', dicionario['Plot'])
print('Premios.....:', dicionario['Awards'])
print('Poster......:', dicionario['Poster'])
print('')


# json.JSONEncoder(skipkeys=False, ensure_ascii=True, check_circular=True, allow_nan=True, sort_keys=False, indent=None, separators=None, default=None)
# Extensible JSON encoder for Python data structures.
# -----------------------------------------------------------------------

# json.JSONDecoder(object_hook=None, parse_float=None, parse_int=None, parse_constant=None, strict=True, object_pairs_hook=None)
# Simple JSON decoder.
# -----------------------------------------------------------------------