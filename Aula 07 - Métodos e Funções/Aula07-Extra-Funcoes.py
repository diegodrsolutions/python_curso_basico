'''
Definindo Funções de uma maneira mais completa

Fonte: https://docs.python.org/3/tutorial/controlflow.html#defining-functions

Diego Mendes Rodrigues
'''

# Definindo valores padrões para argumentos
# Dessa forma a função pode ser chamada com menos argumentos
def sim_ou_nao(pergunta:str, tentativas:int=3, correcao:str='Tente novamente!') -> bool:
    '''Função que retorna True caso a resposta seja s/si/sim e False caso a resposta seja n/na/nao/nã/não'''
    while True:
        resposta = input(pergunta)
        if resposta in ('s','si','sim'):
            return True
        if resposta in ('n','na','nao','nã','não'):
            return False
        tentativas -= 1
        if tentativas < 0:
            print('Resposta inválida')
            return False
        print(correcao)

ira_fazer_backup = sim_ou_nao('Você deseja realizar o backup? (s/n)')
if ira_fazer_backup:
    print('Backup em execução...\nBackup realizado!')
else:
    print('Seus dados não estão sendo backupeados!!!')

print('\n')
ira_fazer_backup = sim_ou_nao('Salvar o arquivo? (sim/não)',1,'Responda sim ou não')
if ira_fazer_backup:
    print('Arquivo salvo!')
else:
    print('Alterações excluídas!')