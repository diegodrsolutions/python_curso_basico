'''
Exercício da Aula 07
Escreva uma função que recebe um objeto de coleção (lista, dicionário, tupla)
e retorna o valor do maior número dentro dessa coleção

Faça outra função que retorna o menor número da coleção

Diego Mendes Rodrigues
'''

# Função que encontra o maior número
def maior(objeto):
    retorno = objeto[0]
    for valor in objeto:
        if valor > retorno:
            retorno = valor
    return retorno

# Função que encontra o menor número
def menor(objeto):
    retorno = objeto[0]
    for valor in objeto:
        if valor < retorno:
            retorno = valor
    return retorno

# Testando as funções criadas
lista = [1,3,9,2,7,10,-2,3]
maior_valor = maior(lista)
menor_valor = menor(lista)
print(lista)
print(maior_valor)
print(menor_valor)

print('\n')
lista = [22.34,11.9,1.3,99.8,212,98,-12.54,301.88,-15.88,1.1]
maior_valor = maior(lista)
menor_valor = menor(lista)
print(lista)
print(maior_valor)
print(menor_valor)

print('\n')
lista = ['a','b','c','d']
maior_valor = maior(lista)
menor_valor = menor(lista)
print(lista)
print(maior_valor)
print(menor_valor)