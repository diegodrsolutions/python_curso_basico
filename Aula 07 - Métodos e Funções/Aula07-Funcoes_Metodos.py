'''
Criando Funções e Módulos no Python

Diego Mendes Rodrigues
'''

# Para criar (ou melhor definir) uma função, usamos def
def soma(numero1, numero2):
    resposta = numero1 + numero2
    return resposta

def tem_7_itens(objeto):
    if len(objeto) == 7:
        return True
    else:
        return False


# Método é quando não temos retorno
def drSolutions():
    print('drSolutions Tecnologia em Informática')


# Usando as funções e métodos
# ----------------------------
print( soma(10,33) )
print( soma(3.14,1.876654) )

print('\n')
drSolutions()

print('\n')
print( tem_7_itens('Diego') )
print( tem_7_itens('Diego M') )
print( tem_7_itens(['a', 'b', 'c', 'd', 'e', 'f']) )
print( tem_7_itens(['a', 'b', 'c', 'd', 'e', 'f', 'g']) )
print( tem_7_itens(['awer', '4eb', 'c6t', 'eed', 'were', '2f', 'erg']) )

if tem_7_itens([1,2,3,4,5,6]):
    print('Realmente tem 7 itens')

if tem_7_itens([1,2,3,4,5,6,7]):
    print('Realmente tem 7 itens')