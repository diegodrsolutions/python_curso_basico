'''
Definindo Variáveis Privadas
Definindo Funções/Métodos Privados

No Python não podemos realmente deixar uma variável ou um método privado
Por convenção, utilizamos as variáveis com _ para demonstrar que ela é privada
e o método com __ para o mesmo sentido

Fonte: https://docs.python.org/3/tutorial/classes.html
'''

# Classe Mapeamento() com a função __atualizar(...) privada
class Mapeamento():
    def __init__(self, item:str=''):
        self.lista_de_itens = []
        if item != '':
            self.__atualizar(item)

    def atualizar(self, item:str):
        self.lista_de_itens.append(item)

    __atualizar = atualizar     # Cópia provada da função atualizar original
                                # dessa forma, __atualizar não pode ser sobrescrita

# Subclasse que sobrescreve a função atualizar(...) mas não sobrescreve __atualizar(...)
class SubMapeamento(Mapeamento):
    def __init__(self,chave='', valor=''):
        Mapeamento.__init__(self)
        if chave != '' and valor != '':
            self.atualizar(chave, valor)

    def atualizar(self, chave, valor):
        # Novo funcionamento para a Função atualizar()
        for item in zip(chave, valor):
            self.lista_de_itens.append(item)

# Instanciando a Classe Mapeamento (com a função privada __atualizar(...))
mapa1 = Mapeamento('chave')
mapa1.atualizar('cadeado')
print(mapa1.lista_de_itens)
print('')

# Instanciando a Classe SubMapeamento que sobrescreve a função atualizar(...)
mapa2 = SubMapeamento('1','A')
mapa2.atualizar('2','B')
print(mapa2.lista_de_itens)