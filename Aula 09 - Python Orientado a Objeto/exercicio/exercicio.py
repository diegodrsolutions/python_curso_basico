'''
Crie um software para Gerenciamento Bancário

Esse software poderá ser capaz de criar e gerenciar: clientes e contas

Cada cliente possui.: nome, cpf, idade

Cada conta possui...: cliente, saldo, limite (negativo)
                      sacar, depositar, consultar saldo

Diego Mendes Rodrigues
'''

# Importando Classes
from cliente import Cliente
from conta import Conta

# Criando um Cliente
cliente = Cliente('Diego Mendes', '220.258.852.-99', 34)
print('CLIENTE')
print('ID.....:', cliente.id)
print('Nome...:', cliente.nome)
print('CPF....:', cliente.cpf)
print('Idade..:', cliente.idade)
id = int(cliente.id)

# Criando a Conta Bancária do Cliente gerado anteriormente
conta_cliente = Conta(id, 500, 1000)
print('\n')
print('CONTA')
print('Cliente ID.:', conta_cliente.cliente_id )
print('Saldo......:', conta_cliente.saldo )
print('Limite.....:', conta_cliente.limite )
print('Pode Sacar.:', conta_cliente.saldo + conta_cliente.limite)

# Tentando realizar um Saque
print('\n')
print('CONTA - Saque')
conta_cliente.sacar(800)
print('Saldo......: R$', conta_cliente.saldo )
print('Limite.....: R$', conta_cliente.limite )
print('Pode Sacar.: R$', conta_cliente.saldo + conta_cliente.limite)

# Tentando realizar outro Saque
print('\n')
print('CONTA - Saque')
conta_cliente.sacar(800)
print('Saldo......: R$', conta_cliente.saldo )
print('Limite.....: R$', conta_cliente.limite )
print('Pode Sacar.: R$', conta_cliente.saldo + conta_cliente.limite)

# Depositando dinheiro na Conta
print('\n')
print('CONTA - Depositar')
conta_cliente.depositar(500)
print('Saldo......: R$', conta_cliente.saldo )
print('Limite.....: R$', conta_cliente.limite )
print('Pode Sacar.: R$', conta_cliente.saldo + conta_cliente.limite)

# Verificando o Saldo e o Limite Disponivel na Conta Bancária
print('\n')
print('CONTA - Saldo')
conta_cliente.ver_saldo()