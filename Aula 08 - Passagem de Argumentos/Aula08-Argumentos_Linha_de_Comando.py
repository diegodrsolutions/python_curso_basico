'''
Argumentos de Linha de Comando
Muito util para fazer scripts executados em modo texto

Diego Mendes Rodrigues
'''

# Importando a biblioteca sys que possui funções que conversam com o sistema operacional
import sys

# Funções criadas para serem usadas no script
def soma(n1:float,n2:float) -> float:
    return float(n1)+float(n2)

def subtrai(n1:float,n2:float) -> float:
    return float(n1)-float(n2)

# Lista dos argumentos recebidos
argumentos = sys.argv   # arg1 -> metodo / arg2 -> n1 / arg3 -> n2
#print(argumentos)

if argumentos[1] == "help":
    print('Use da seguinte forma:')
    print(argumentos[0],'<opção> <numero 1> <numero 2>')
    print('\t<opção> - soma / sub')
    print('\t<numero 1> - defina um numero')
    print('\t<numero 2> - defina outro numero')
    quit(0)

if len(argumentos) != 4:
    print('São necessários 3 argumentos, use help caso precise de ajuda')
    quit(1)

if argumentos[1] not in ("soma", "sub"):
    print('O primeiro argumento deve ser: help, soma, sub')
    quit(1)

resposta = 0
if argumentos[1] == "soma":
    resposta = soma(argumentos[2],argumentos[3])
elif argumentos[1] == "sub":
    resposta = subtrai(argumentos[2], argumentos[3])

print(resposta)