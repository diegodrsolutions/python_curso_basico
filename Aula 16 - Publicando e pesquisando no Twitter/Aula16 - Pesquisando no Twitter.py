'''
Pesquisando informações no Twitter

http://dev.twitter.com/rest/public
The REST APIs provide programmatic access to read and write Twitter data. Create a new Tweet, read user profile and
follower data, and more. The REST API identifies Twitter applications and users using OAuth;
responses are in JSON format.

Autenticação
Na API do Twitter precisamos Autenticar para trabalhar com o Twitter
Conta confirmada e com Telefone

diego@drsolutions.com.br
hdG7@f1c!4

aula_python_drsolutions
Consumer Key (API Key):         WluEfSlQIRUoVMQyPrUNfP58E
Consumer Secret (API Secret):   Cru3A8TEdlpWn4RKrGpojYiQgbUEgSdnfOm3ugAjzGnlvsygdE

Access Token:                   751409640269869056-KtJbZKKmin8lDQ9EZHcBlqH0CvS7MBc
Access Token Secret:            GT9C9ds87fJoTyLOeZpXFzpbu7O1Lt2U2fiymidnXPHEt


OAuth2
pip install oauth2

Diego Mendes Rodrigues
'''

# Twitter usa autenticação OAuth
import oauth2
import json

import urllib.parse

#import pprint

# Autencicação do Twitter
Consumer_Key    = 'WluEfSlQIRUoVMQyPrUNfP58E'                           # Usuário
Consumer_Secret = 'Cru3A8TEdlpWn4RKrGpojYiQgbUEgSdnfOm3ugAjzGnlvsygdE'  # Senha
Token_Key       = '751409640269869056-KtJbZKKmin8lDQ9EZHcBlqH0CvS7MBc'  # Usuário
Token_Secret    = 'GT9C9ds87fJoTyLOeZpXFzpbu7O1Lt2U2fiymidnXPHEt'       # Senha

# Autenticando
consumidor = oauth2.Consumer(Consumer_Key, Consumer_Secret)
token      = oauth2.Token(Token_Key, Token_Secret)

cliente    = oauth2.Client(consumidor, token)   # Criando o Cliente da Autenticação
# Com esse cliente podemos fazer as requisições, como faziamos com o requests

texto = input('Qual texto devo procurar: ')
texto_sem_espacos = urllib.parse.quote(texto, safe='')
if len(texto) <= 0:
    exit()

req = cliente.request('https://api.twitter.com/1.1/search/tweets.json?q='+texto_sem_espacos+'&lang=pt')
# req é uma tupla (objeto e bytes)

# Transformando em str
decodificar = req[1].decode()

# Tranaformando em um dicionário
dicionario = json.loads(decodificar)

# Essa resposta do statuses me retorna uma lista
respostas = dicionario['statuses']

for linha in respostas:
    print('Nome:', linha['user']['name'])
    print('ID..:', linha['user']['id_str'])
    print('@'+linha['user']['screen_name'])
    print('https://twitter.com/'+linha['user']['screen_name'])
    print('')
    print('http://twitter.com/' + linha['user']['id_str'] + '/status/' + linha['id_str'])
    print('')
    print('Tweet:')
    print(linha['text'])
    print('')
    print('-------------------------------------------------------')
    print('')