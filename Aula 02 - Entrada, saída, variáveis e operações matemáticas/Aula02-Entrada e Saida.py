'''
Entrada e Saída no Python

Diego Mendes Rodrigues
'''

# Imprimindo informações na tela
print('Exibindo informações (uma string) na tela')
print('Função print sempre exibe strings')
print('Cada print pula uma linha, mas podemos usar \\n, exemplo:')
print('Diego\nMendes\nRodrigues')
print('\n')

# Incluindo informações nas variáveis
# Python automaticamente adota o tipo para cada variável
nome='Diego Mendes' # Python percebeu que é um texto (string)
print(nome)         # Posso colocar a variável nome no print, pois é uma string

# Exibindo o tipo da variável
tipo_nome = type(nome)
print(tipo_nome)     # Objeto do tipo str

idade = 34
print(idade)
tipo_idade = type(idade)
print(tipo_idade)   # Objeto do tipo int
                    # Python converteu a idade em string para imprimir usando o print

altura = 1.70
tipo_altura = type(altura)
print(altura)
print(tipo_altura)  # Objeto do tipo float

# Concatenando
print('\n')
print(nome, 'tem', idade, 'anos com a altura de', altura, 'metros')

# Precisamos converter INT e FLOAT para STR, além de colocar os espaços
frase = nome + ' tem ' + str(idade) + ' anos com a altura de ' + str(altura) + ' metros'
print('\n')
print(frase)