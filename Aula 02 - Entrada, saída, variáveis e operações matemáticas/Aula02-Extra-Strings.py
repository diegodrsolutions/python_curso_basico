'''
Métodos e funções com Strings

Fonte: https://www.youtube.com/watch?v=cC9UnbxxKpY

Diego Mendes Rodrigues
'''

frase = 'Diego Mendes Rodrigues'
print( frase )


# Imprimindo o 3o caracter (0,1,2)
print( frase[2] )


# Imprimindo do 1o ao 5o caracter (0,1,2,3,4)
print( frase[0:5] )
print( frase[:5] )


# Imprimindo do 7o ao 12o
print( frase[6:12] )


# Imprimindo os últimos 5 caracteres
print( frase[-5:] )


# Imprimindo os últimos 9 caracteres
print( frase[-9:] )


print('\n')

# Imprimindo todos os caracteres
print( frase[::] )


# Imprimindo todos os caracteres, com passo 2
print( frase[::2] )


# Imprimindo todos os caracteres, com passo 3
print( frase[::3] )


print('\n')

# Invertendo
print( frase[::-1] )

print('\n')


# Testando se a frase começa com uma palavra
print( frase.startswith('Diego') )
print( frase.startswith('Regina') )

print('\n')


# Testando se a frase termina com uma palavra
print( frase.endswith('Rodrigues') )
print( frase.endswith('Ruiz') )

print('\n')


# Maiuscula / Minuscula
print( frase.upper() )
print( frase.lower() )

print('\n')


# Testando os .is....()
print( frase.isalnum() )
print( frase.isalpha() )
print( frase.isdecimal() )
print( frase.isdigit() )
print( frase.isnumeric() )
print( frase.islower() )
print( frase.isupper() )
print( frase.isprintable() )
print( frase.isspace() )
print( frase.istitle() )

print('\n')


# Quebrando a String frase nos espaços
nome = frase.split(' ')
print( nome )
print( nome[0] )
print( nome[1] )
print( nome[2] )

print('\n')


# Captalize()
cliente = 'regina marcia ruiz mendes rodrigues'
print( cliente )
print( cliente.capitalize() )
print( cliente.title() )

print('\n')


# find() - Em que posição está o Ruiz?
print( cliente.find('ruiz') )

print('\n')


# format(o)
num1 = 10
num2 = 7
soma = num1 + num2
print( 'A Soma de {0} com {1} é igual a {2}!'.format(num1, num2, soma) )

print('\n')


# Substituindo com replace()
print( cliente )
print( cliente.replace('ruiz','neau') )
print( cliente.replace(' mendes','') )

print('\n')


# Quebrando Linhas
fornecedor = 'drSolutions\nCNPJ: 51.009.987/0001-02\nTelefone: (11) 3452-9882'
print(fornecedor)
fornecedor_lista = fornecedor.splitlines()
print(fornecedor_lista)
print(fornecedor_lista[0])
print(fornecedor_lista[1])
print(fornecedor_lista[2])

print('\n')


# zero fill .zfill(x)
altura = '22'
print(altura)
print(altura.zfill(5))

print('\n')


# Juntando uma lista com join
frutas = ['abacaxi', 'manga', 'pera', 'uva']
print( frutas )
print( '-'.join(frutas) )
print( ' | '.join(frutas) )
print( ', '.join(frutas) )