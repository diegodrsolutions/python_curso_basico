'''
Exercício da Aula 02
Montar um formulário que pergunte: nome, CPF, endereço, idade, altura e telefone
Imprimir essas entradas em um relatório

Diego Mendes Rodrigues
'''

# Formulário de entrada de dados
nome        = input('Qual seu nome.....: ')
cpf         = input('Qual seu CPF......: ')
endereco    = input('Qual seu endereço.: ')
telefone    = input('Qual seu telefone.: ')
idade       = input('Qual sua idade....: ')
altura      = input('Qual sua altura...: ')

# Exibindo os resultados
print('\n')
print('RESULTADOS\n----------')
print('Nome.....:',nome)
print('CPF......:',cpf)
print('Endereço.:',endereco)
print('Telefone.:',telefone)
print('Idade....:',idade)
print('Altura...:',altura,'metros')