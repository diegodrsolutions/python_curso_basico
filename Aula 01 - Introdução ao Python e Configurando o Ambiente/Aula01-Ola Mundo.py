'''
Primeiro programa em Python 3.5

Diego Mendes Rodrigues
'''

# Exibindo informações iniciais na tela
print('Olá mundo!')
print('\nMeu primeiro programa do curso')

# Definindo variáveis
a = 10
b = 20
c = 30

# Exibindo as variáveis no Console
print('\nVariáveis: a =', a,', b =',b,', c =',c)
print('a+b =', a+b)
print('b*c =', b*c)
print('c-b =', c-b)

# Saindo do Script
quit()